use crate::dictionary::DictionaryEntry;
use std::fs::File;
use std::io::{BufRead, BufReader, Error};
use unicode_normalization::char::is_combining_mark;
use unicode_normalization::UnicodeNormalization;

pub fn read(filename: &'static str) -> Result<Vec<DictionaryEntry>, Error> {
    let f = File::open(filename)?;
    let reader = BufReader::new(&f);
    Ok(reader
        .lines()
        .map(|line| {
            let line_string = line.unwrap();
            let mut split = line_string.split('|');

            let secwepemc = split.next().unwrap().to_owned();
            let english = split.next().unwrap().to_owned();
            let normalized_secwepemc = secwepemc
                .nfd()
                .filter(|c| !is_combining_mark(*c))
                .collect::<String>()
                .to_lowercase();
            let normalized_english = english
                .nfd()
                .filter(|c| !is_combining_mark(*c))
                .collect::<String>()
                .to_lowercase();
            DictionaryEntry {
                secwepemc,
                english,
                normalized_secwepemc,
                normalized_english,
            }
        })
        .collect::<Vec<DictionaryEntry>>())
}
