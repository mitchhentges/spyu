use std::io::Cursor;
use std::str::FromStr;

use tiny_http::{Header, Method, Request, Response, Server};

use crate::dictionary::{DictionaryEntry, DictionaryEntryDto, DictionarySearchResult};

mod dictionary;
mod file_import;

struct DictionaryMatch<'a> {
    entry: &'a DictionaryEntry,
    border_spaces: usize,
    match_length: usize,
}

impl<'a> DictionaryMatch<'a> {
    fn new(
        to_check: &String,
        to_check_length: usize,
        entry: &'a DictionaryEntry,
    ) -> DictionaryMatch<'a> {
        let strings = [
            &entry.secwepemc,
            &entry.english,
            &entry.normalized_secwepemc,
            &entry.normalized_english,
        ];
        let best_match = strings
            .iter()
            .map(|target| (target, spaces_around(to_check, to_check_length, target)))
            .max_by_key(|target_match| target_match.1)
            .expect("There should always be at least one element in the 'strings' array");
        DictionaryMatch {
            entry,
            border_spaces: best_match.1,
            match_length: best_match.0.chars().count(),
        }
    }
}

fn spaces_around(to_check: &String, to_check_length: usize, target: &str) -> usize {
    let mut space_count = 0;

    if let Some(start_index) = target.find(to_check) {
        if start_index == 0 || target.chars().nth(start_index - 1).unwrap_or(' ') == ' ' {
            space_count += 1;
        }

        if target
            .chars()
            .nth(start_index + to_check_length)
            .unwrap_or(' ')
            == ' '
        {
            space_count += 1;
        }
    }

    space_count
}

fn look_up(dictionary: &[DictionaryEntry], word: &str) -> Vec<DictionaryEntryDto> {
    let to_check = &(*word).to_owned().to_lowercase();
    let to_check_length = to_check.chars().count();

    if to_check.is_empty() {
        vec![]
    } else {
        let dictionary_entries = dictionary
            .iter()
            .filter(|entry| {
                entry.secwepemc.contains(to_check)
                    || entry.english.contains(to_check)
                    || entry.normalized_secwepemc.contains(to_check)
                    || entry.normalized_english.contains(to_check)
            })
            .map(|entry| DictionaryMatch::new(to_check, to_check_length, entry));

        // Only sort entries if the requested string is big enough to limit results
        if to_check_length > 1 {
            let mut entry_vector = dictionary_entries.collect::<Vec<DictionaryMatch>>();
            entry_vector.sort_by(|a, b| {
                // Sort first by the match with more border spaces.
                // Next, sort by the match with the smaller description
                if a.border_spaces == b.border_spaces {
                    a.match_length.cmp(&b.match_length)
                } else {
                    b.border_spaces.cmp(&a.border_spaces)
                }
            });
            entry_vector
                .into_iter()
                .take(5)
                .map(|dictionary_match| {
                    DictionaryEntryDto::new(
                        dictionary_match.entry.secwepemc.clone(),
                        dictionary_match.entry.english.clone(),
                    )
                })
                .collect::<Vec<DictionaryEntryDto>>()
        } else {
            dictionary_entries
                .take(5)
                .map(|dictionary_match| {
                    DictionaryEntryDto::new(
                        dictionary_match.entry.secwepemc.clone(),
                        dictionary_match.entry.english.clone(),
                    )
                })
                .collect::<Vec<DictionaryEntryDto>>()
        }
    }
}

#[derive(Debug)]
enum Error {
    Serde(serde_json::Error),
    SerializeHeader,
}

impl From<serde_json::Error> for Error {
    fn from(value: serde_json::Error) -> Self {
        Error::Serde(value)
    }
}

fn header(header: &str) -> Result<Header, Error> {
    Header::from_str(header).map_err(|_| Error::SerializeHeader)
}

fn handle_req(
    req: &Request,
    dictionary: &[DictionaryEntry],
) -> Result<Response<Cursor<Vec<u8>>>, Error> {
    if req.method() != &Method::Get {
        return Ok(
            Response::from_string("Invalid request method, 'GET' expected").with_status_code(400),
        );
    }

    let url = req.url();
    let word = match percent_encoding::percent_decode(url.split_at(1).1.as_bytes()).decode_utf8() {
        Ok(word) => word,
        Err(e) => return Ok(Response::from_string(e.to_string()).with_status_code(400)),
    };

    let entries = look_up(dictionary, &word);
    let total_results = entries.len();
    let out = DictionarySearchResult::new(1, entries);
    println!("\"{}\" -> {} results", word, total_results);

    Ok(Response::from_string(serde_json::to_string(&out)?)
        .with_header(header("content-type: application/json; charset=utf-8")?)
        .with_header(header("access-control-allow-origin: *")?)
        .with_header(header("access-control-allow-methods: *")?))
}

fn main() {
    let dictionary = file_import::read("dictionary").unwrap();
    let server = Server::http("0.0.0.0:16845").unwrap();
    for req in server.incoming_requests() {
        let res = match handle_req(&req, &dictionary) {
            Ok(res) => res,
            Err(e) => {
                eprintln!("{:?}", e);
                Response::from_string("").with_status_code(500)
            }
        };
        req.respond(res).unwrap();
    }
}
