# Spyu

Translator between English and [Secwepemctsín](https://en.wikipedia.org/wiki/Shuswap_language).

## Running in Production

This project requires [the Rust Programming Language](https://www.rust-lang.org/).

1. From a build machine, do `cargo build --release`
2. Copy `./static/` to your server, such as to `/usr/share/nginx/spyu.ca`
3. Copy `./target/release/spyu-server` and `dictionary` to your server, such as to `/opt/spyu`
4. Run `spyu-server` on the server. An example with `systemd` is:
    ```
    [Unit]
    Description=Dictionary lookup server for Spyu
    Requires=network.target
    
    [Service]
    User=spyu
    WorkingDirectory=/opt/spyu
    ExecStart=/opt/spyu/spyu-server
    
    [Install]
    WantedBy=multi-user.target
    ```

5. If you have a reverse proxy, configure it. An example `nginx` config is:

    ```
    server {
        listen 80;
        server_name www.spyu.ca spyu.ca;
        root /usr/share/nginx/spyu.ca;
    
        location /api/ {
            proxy_pass http://127.0.0.1:16845/;
            proxy_redirect off;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
    ```