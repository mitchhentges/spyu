var server = "https://spyu.ca/api/";

var search, results;
function init() {
    search = document.getElementById("search");
    results = document.getElementById("results");
    search.addEventListener("input", queryChanged);
}

function queryChanged(e) {
    var query = e.target.value;
    if (query === "") {
        return;
    }

    var dictionaryRequest = new XMLHttpRequest();
    dictionaryRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            updateEntries(JSON.parse(dictionaryRequest.responseText))
        }
    };
    dictionaryRequest.open("GET", server + query);
    dictionaryRequest.send();
}

function updateEntries(json) {
    while (results.firstChild) {
        results.removeChild(results.firstChild);
    }

    json.entries.forEach(function(entry) {
        var node = document.createElement("div");
        node.className = "result";
        var secwepemc = document.createElement("div");
        secwepemc.className = "secwepemc";
        secwepemc.appendChild(document.createTextNode(entry.secwepemc));
        node.appendChild(secwepemc);
        var english = document.createElement("div");
        english.className = "english";
        english.appendChild(document.createTextNode(entry.english));
        node.appendChild(english);
        results.appendChild(node);
    });
}

document.addEventListener("DOMContentLoaded", init);
